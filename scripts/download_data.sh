#!/bin/bash

echo "Start downloading data..."
poetry run dvc pull
echo "Downloading...done"

echo "Starting unzip..."
sudo apt-get install unzip
unzip -qq data/alphabet.zip -d data/
unzip -qq data/asl_alphabet_train.zip -d data/
rm -r data/__MACOSX/
rm data/alphabet.zip 
rm data/asl_alphabet_train.zip
echo "unzip...done"