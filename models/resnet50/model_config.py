from dataclasses import dataclass

from torchvision.transforms import Compose, Normalize, Resize, ToTensor


@dataclass
class ModelConfig:
    TRANSFORMS = Compose(
        [
            Resize(224),
            ToTensor(),
            Normalize(
                mean=[0.485, 0.456, 0.406], 
                std=[0.229, 0.224, 0.225]
            ),
        ]
    )
    DEVICE = 'cpu'#"cuda" if torch.cuda.is_available() else "cpu"
    TRAIN_DATA_PATH = "data/asl_alphabet_train"
    TEST_DATA_PATH = "data/alphabet"
