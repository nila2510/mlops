from clearml import Task

class ClearML:
    def __init__(self, project_name, task_name):
        # создаем таску
        self.task = Task.init(project_name=project_name, task_name=task_name)

        # создаем объект класса логгер
        self.logger = self.task.get_logger()

    def report_scalar(self, title, series, value, iteration):
        # логируем метрики
        self.logger.report_scalar(title=title, series=series, value=value, iteration=iteration)

    def upload_model(self, model, model_name):
        # сохраняем модель в clearml-хранилище
        self.task.upload_artifact(name=model_name, artifact_object=model)